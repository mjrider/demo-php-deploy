<?php

namespace Deployer;

require 'recipe/common.php';
require 'recipe/rsync.php';
require 'recipe/newrelic.php';

set(
    'newrelic_deploy_user',
    'Deployment'
);

$apikey = getenv('NEWRELIC_API_KEY');
if ($apikey != '') {
    set('newrelic_app_id', getenv('NEWRELIC_APP_ID')??'');
    set('newrelic_api_key', $apikey);
}

set('keep_releases', 10);
set('ssh_type', 'native');
set('ssh_multiplexing', true);

set(
    'shared_dirs',
    [
        'logs',
    ]
);

set(
    'writable_dirs',
    [
        'logs',
    ]
);

/* Deze setting wil je eigenlijk gebruiken */
//set('writable_mode', 'acl');
/* speciaal voor de demo */
set('writable_mode', 'chmod');


set('writable_use_sudo', false);
set('shared_files', []);

set(
    'buildenv',
    function () {
        $stage = null;
        if (input()->hasArgument('stage')) {
            $stage = input()->getArgument('stage');
        }
        if (!isset($stage)) {
            $stage = getenv('CI_ENVIRONMENT_NAME');
        }
        return $stage;
    }
);

set('default_stage', getenv('CI_ENVIRONMENT_NAME'));

set(
    'environment',
    function () {
        $environment = get('buildenv');
        switch ($environment) {
            case 'production':
                $environment = 'prod';
                break;
            case 'acceptance':
                $environment = 'accept';
                break;
            case 'testing':
                $environment = 'testing';
                break;
            case 'development':
                $environment = 'dev';
                break;
            default:
                throw new \RuntimeException("Unknown environment [" . $environment . "]");
        }

        return $environment;
    }
);

inventory('deploy.yml');

set('rsync_src', __DIR__);
set('rsync_dest', '{{release_path}}');

set(
    'rsync',
    [
    'exclude' => [
        '.git',
        'deploy.php',
        'deploy.yml',
        '.gitlab-ci.yml',
    ],
    'exclude-file' => false,
    'include' => [],
    'include-file' => false,
    'filter' => [],
    'filter-file' => false,
    'filter-perdir' => false,
    'flags' => 'rzcE',
    'options' => [
        'delete',
        'delete-after',
        'delay-updates',
        'ignore-times',
        'delete-excluded',
        'links'
    ],
    'timeout' => 60,
    ]
);

task(
    'build:env',
    function () {
        $buildenv = get('buildenv');
	if (testLocally('[ -e .env.' . $buildenv . ']')) {
	    runLocally("cp .env." . $buildenv . " .env");
	}
    }
)->desc('Build enviroment symlink')->once();

task(
    'deploy',
    [
	'deploy:info',
        'deploy:prepare',
        'deploy:lock',
        'deploy:release',
        'build:env',
        'rsync',
        'deploy:clear_paths',
        'deploy:shared',
        'deploy:writable',
        'deploy:symlink',
        'deploy:unlock',
        'cleanup',
    ]
)->desc('Deploy project');

task('newrelic:notify')
    ->once()
    ->onStage('production');

// prevent errors when  there is no newrelic api key
if ($apikey != false) {
    after('deploy:symlink', 'newrelic:notify');
}
