# php gitlab demo project
Dit project is een simpel voorbeeld met aannames voor complexere projecten zal er in de build fase meer of anders de artifacts gebouwt moeten worden voor de deployment
Aannames
- Er is geen build process voor javascript/css
- PHP heeft alleen maar de vendor map nodig
- Er is geen framework
- logging gaat naar file, een extern projess zorgt dat dat in je centrale logging komt

## Gitlab pipeline
### preflight
Containers die de sanity van het project proberen te bewaken.

### build
Bouw de vendor map, met en zonder dev utils er in, met devutils os speciaal voor de tests, zonder is voor je deployment

### test
phpunit draaien, of andere test jobs mogen hier komen

### deploy
Deployed naar accept en production op basis van de version tag
vx.y.z == production
vx.y.z-rcN == acceptance

## deployment in het echt
De deployment tool verwacht de volgende directory structuur
* `/some/path/to/project-root/`
  De deploy gebruiker (nee niet root) is de eigenaar van deze folder

Onder deze map maakt deze gebruiker de volgende structuur zelf aan:
|map            | beschrijving                                           |
|---------------|--------------------------------------------------------|
| ./current     | symlink naar de huidige release |
| ./releases    | folder waar de releases in komen, een map per release |
| ./.dep        | folder voor deployer files |
| ./shares      | gedeelte bestanden/folders worden uit de release folder gedeleted en hier heen gesymlinkt. |

met behulp van setfacl worden de permissies goed gezet zodat de gebruiker waar apache onder draait mag schrijven in de folders die writeable worden gemarkeerd

### Inrichting webserver
Met dit demo project als voorbeeld zou de webroot van de webserver `/some/path/to/project-root/current/public/` worden

## Beperkingen in php
Om beperkingen in php te omzeilen wil je paden altijd absoluut hebben baseer je path altijd op `__DIR__` of `__FILE__` zodat de caching van php niet  de verkeerde locatie terug geeft uit z'n cache


## Inrichting