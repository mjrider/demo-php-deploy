<?php

error_reporting(E_ALL & ~E_DEPRECATED);
date_default_timezone_set('Europe/Amsterdam');
session_set_cookie_params(0, '/', null, true, true);

use Monolog\ErrorHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\NewRelicHandler;
use Monolog\Handler\StreamHandler as MonologStreamHandler;
use Monolog\Logger;

function logger()
{
    global $logger;
    return $logger;
}

require_once __DIR__ . '/vendor/autoload.php';

$formatter = new LineFormatter(LineFormatter::SIMPLE_FORMAT, LineFormatter::SIMPLE_DATE);
$formatter->includeStacktraces(true);

$stream = new MonologStreamHandler(__DIR__ . '/logs/app.log');
$stream->setFormatter($formatter);

$logger = new Logger('logger');
$logger->pushHandler($stream);
if (extension_loaded('newrelic')) {
    $logger->PushHandler(new NewRelicHandler());
}

ErrorHandler::register($logger);

// Check if the New Relic PHP Module is loaded
if (extension_loaded('newrelic')) {
    // Set New Relic app name
    newrelic_set_appname('myAwesomeProject');
}

// Load configuration into environment
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeload();

// production doesnt want debug logging
if (($_ENV['APP_ENV'] ?? 'dev') === "production") {
    $stream->setLevel(LOGGER::INFO);
}
